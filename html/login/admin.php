<?php
session_start();

$errmsg = "";

$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    if(!isset($_SESSION["UID"]))
    {
        header("Location:index.php");
    }

    if(isset($_POST["submit"])){


        if(empty($_POST["txtFName"])){
            $errmsg = "Name is required";
        }else{
            $FName = $_POST["txtFName"];
        }


        if(empty($_POST["txtEmail"])){
            $errmsg = "Email is required";
        }else{
            $Email = $_POST["txtEmail"];
        }


        if(empty($_POST["txtPassword"])){
            $errmsg = "Password is required";
        }else{
            $Password = $_POST["txtPassword"];
        }

        if($Password != $_POST["txtPassword2"]){
            $errmsg = "Passwords do not match!";
        }


        if(empty($_POST["txtRole"])){
            $errmsg = "Role is required";
        }else{
            $Role = $_POST["txtRole"];
        }


        if($errmsg == ""){
            // DO DB WORK
            //$errmsg = "do sql work";

            include('../Template/db_conn.php');
            try{

                $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
                $sql = $db->prepare("
                insert into memberLogin(memberName, memberEmail, memberPassword, RoleID, memberKey)
                VALUE (:Name, :Email, :Password, :RID, :Key)
            ");

                $sql->bindValue(':Name', $FName);
                $sql->bindValue(':Email', $Email);
                $sql->bindValue(':Password', md5 ($Password.$key));
                $sql->bindValue(':RID', $Role);
                $sql->bindValue(':Key', $key);
                $sql->execute();
            }
            catch(PDOException $e){
                $error = $e->getMessage();
                echo "Error: $error";
            }

            $FName = "";
            $Email = "";
            $Password = "";
            $Role = "";

            $errmsg = "Member Added to the Database";
        }


        // echo "We are here";
        // exit();

    }




?>




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heather's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <h1>Admin Page</h1>
    <form method="post">
        <h3 id="error"><?=$errmsg?></h3>
       <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">  Add New Member </th>
            </tr>
            <tr height="50">
                <th>Full Name</th>
                <td><input type="text" name="txtFName" id="txtFName" value="<?=$FName?>" required /></td>
            </tr>
           <tr height="50">
               <th>Email</th>
               <td><input type="text" name="txtEmail" id="txtEmail" value="<?=$Email?>" required /></td>
           </tr>
            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="txtPassword" id="txtPassword" required /></td>
            </tr>
           <tr height="50">
               <th>Retype Password</th>
               <td><input type="password" name="txtPassword2" id="txtPassword2" required /></td>
           </tr>

           <tr height="50">
               <th>Role</th>
               <td>
                   <select id="txtRole" name="txtRole">
                       <option value="1">Admin</option>
                       <option value="2">Operator</option>
                       <option value="3">Member</option>
                   </select>
               </td>
           </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" value="Add New Member" name="submit"/>
                </td>
            </tr>

        </table>


    </form>

</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>