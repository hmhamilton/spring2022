<?php
session_start();

    if( isset($_POST['txtEmail']) && !empty($_POST['txtEmail'])
        && isset($_POST['txtPassword']) && !empty($_POST['txtPassword']))
    {
        $Email = $_POST["txtEmail"];
        $password = $_POST["txtPassword"];
        $errmsg = "";



        include('../Template/db_conn.php');

        try{

            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);

            $sql = $db->prepare("SELECT memberID, memberPassword, memberKey, RoleID FROM memberLogin where memberEmail = :Email;");
            $sql->bindValue(':Email', $Email);
            $sql->execute(); // baking order
            $row = $sql->fetchAll(); // delivery

            if($row != null) {


                $hashedPassword = md5($password . $row["memberKey"]);

                if ($hashedPassword == $row["memberPassword"]) {
                    $_SESSION["UID"] = $row["memberID"];
                    $_SESSION["Role"] = $row["RoleID"];
                    if ($row["RoleID"] == 1) {
                        header('Location:admin.php');
                    } else {
                        header('Location:member.php');
                    }
                }else{
                    $errmsg="Wrong username or password";
                }
            }else{
                $errmsg="Wrong username or password";
            }


        }
        catch (PDOException $e)
        {
           $error = $e->getMessage();
            echo "Error: $error";
        }








        if(strtolower($userName) == "admin" && $password == "p@ss"){
            $_SESSION["UID"] = 1;
             header('Location:admin.php');
        }
        else
        {
            if(strtolower($userName) == "user" && $password == "p@ss"){
                header('Location:member.php');
            }
            else
            {
                $errmsg = "Invalid Username or Password";
            }

        }


    }


?>






<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />


</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <form method="post">
        <h3 id="error"><?=$errmsg?></h3>

        <table border="1" width="80%">

            <tr height="100">
              <th colspan="2">  User Login </th>
            </tr>
            <tr height="50">
                <th>Email</th>
                <td><input type="text" name="txtEmail" id="txtEmail"  /></td>
            </tr>

            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="txtPassword" id="txtPassword"  /></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" value="Login"/>

                </td>
            </tr>

        </table>


    </form>


</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>