<?php
    $SECONDS_PER_MIN = 60;
    $SECONDS_PER_HOUR = 60 * $SECONDS_PER_MIN;
    $SECONDS_PER_DAY = 24 * $SECONDS_PER_HOUR;
    $SECONDS_PER_YEAR = 365 * $SECONDS_PER_DAY;

    $NOW = time();
    $NEXT_DECADE = mktime(0,0,0,1,1,2030);

    $seconds = $NEXT_DECADE - $NOW;

    // how many years?

    $years =  floor($seconds / $SECONDS_PER_YEAR);

    // remove the years and seconds from total seconds

    $seconds = $seconds - ($SECONDS_PER_YEAR * $years);

    // how many days?

    $days = floor($seconds/$SECONDS_PER_DAY);

    // remove days in seconds from total seconds

$seconds = $seconds - ($SECONDS_PER_DAY * $days);


// how many hours?

$hours = floor($seconds/$SECONDS_PER_HOUR);

// remove hours in seconds from total seconds

$seconds = $seconds - ($SECONDS_PER_HOUR * $hours);

// how many minutes??

$minutes = floor($seconds/$SECONDS_PER_MIN);

// remove minutes in seconds from total seconds

$seconds = $seconds - ($SECONDS_PER_MIN * $minutes);






?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Countdown Timer</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
</head>
<body>
<header>
    <?php include('../template/header.php'); ?>
</header>

<nav>
    <?php include('../template/nav.php'); ?>

</nav>

<main>
    <h1>Countdown Timer</h1>
    <p>Now: <?= $NOW ?>
    <p> <?= date("Y-m-d H:i:s")?></p>
    <p>   |Next Decade: <?= $NEXT_DECADE ?></p>
    <p>   |Years: <?= $years ?></p>
    <p>   | Days: <?= $days ?></p>
    <p>   | Hours: <?= $hours ?></p>
    <p>   | Minutes: <?= $minutes ?></p>
    <p>    | Seconds: <?= $seconds ?></p>

</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>


</body>
</html>