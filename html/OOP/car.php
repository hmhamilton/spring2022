<?php


class Car
{
    /**
     * @var string
     */
    public $color;

    /**
     * @var string
     *
     * Chevy, Dodge, Toyota
     */

    public $make;
    /**
     * @var string
     *
     * Corvette, Viper, Mustang, Supra
     */

    public $model;

    /**
     * @var int
     *
     * 2021, 2022
     */

    public $year;

    /**
     * @var string
     *
     * parked, forward, reverse
     */

    public $status;

    /**
     * Car constructor.
     */

    function __construct()
    {
        $this->status = 'parked';


    }

    /**
     *  put the car in drive
     */

    function drive()
    {
        echo "The car is moving forward.<br/><br/>";
        $this->status = 'drive';
    }

    /**
     * put the car in reverse
     */
    function reverse()
    {
        echo "The car is moving backwards.<br/><br/>";
        $this->status = "reverse";

    }

    /**
     * put the car in park
     */

    function park()
    {
        echo "The car is in park.<br/><br/>";
        $this->status = "parked";
    }

    /**
     * set the color of the car
     *
     * @param string $color the color of the car.(red, blue, green, etc)
     */
    function setColor($color)
    {
        $valid_colors = ['red', 'blue', 'green', 'black'];

        if(!in_array(strtolower($color), $valid_colors))
        {
            echo "color is invalid, try a different one!<br/><br/>";
            exit;

        }
        else
        {
            $this->color = $color;
        }
        $this->color = $color;
    }

    /**
     * returns the class color property
     * @return string
     */

    function getColor ()
    {
        return $this->color;
    }


}// end of class


$my_first_car = new Car();
$my_first_car->setColor('green');
$my_first_car->make = "Dodge";
$my_first_car->model = "Shadow";
$my_first_car->year = 1994;

echo "My first car was a $my_first_car->year $my_first_car->model $my_first_car->make<br/><br/>";

$my_first_car->drive();
$my_first_car->reverse();
$my_first_car->park();







?>