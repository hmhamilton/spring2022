<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Loops</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <h1>Loops and String functions demo</h1>

    <h3> Basic Vars and Stuff</h3>
    <?php
        $number = 100;
        print $number;
        echo "<br><strong>".$number."</strong>";
        echo "<br><strong>";
        echo "<br><strong>";
    echo $number;
    echo "</strong>";

    $result = "<br><strong>";


    ?>

    <h3>math</h3>
    <?php
    $number_1 = 100;
    $number_2 = 50;
    $result_1 = $number_1 + $number_2;
    $result_2 = $number_1 + $number_3;

    echo "<br>".result_1;
    echo "<br>".result_2;
    ?>

    <h3>while loop</h3>

    <?php

    $i = 1;

    while ($i < 7) {
        echo "<h$i> Howdy World </h$i>";
        $i++;
    }
    ?>


    <h3> do while loop</h3>
    <?php
    $i = 6;

    do {
         echo "<h$i> Howdy World </h$i>";
        $i--;
    }
    while ($i > 0);

    ?>



    <h3> For loop</h3>
    <?php
    for($i = 1; $i < 6; $i++) {
        echo "<h$i> Howdy World </h$i>";
    }
    ?>

    <h3> More string fun</h3>

    <?php
    $full_name = 'Bob Smith';

    $position = strpos($full_name, " ");
    echo "<br> The space position is in position $position";
    echo "<br>".$full_name;
    echo "<br>".strtoupper($full_name);
    // can also do strtolower also

    $name_parts = explode(" ", $full_name);
    echo "<br>First Name: ".$name_parts[0];
    echo "<br>Last Name: ".$name_parts[1];

    ?>




</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>