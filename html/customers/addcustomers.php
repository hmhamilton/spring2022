<?php

if(
    isset($_POST['CustomerID']) && !empty($_POST['CustomerID'])
    && isset($_POST['FirstName']) && !empty($_POST['FirstName'])
    && isset($_POST['LastName']) && !empty($_POST['LastName'])
    && isset($_POST['Address']) && !empty($_POST['Address'])
    && isset($_POST['City']) && !empty($_POST['City'])
    && isset($_POST['State']) && !empty($_POST['State'])
    && isset($_POST['Zip']) && !empty($_POST['Zip'])
    && isset($_POST['Phone']) && !empty($_POST['Phone'])
    && isset($_POST['Email']) && !empty($_POST['Email'])
    && isset($_POST['Password']) && !empty($_POST['Password'])
) {


    $customer= $_POST['CustomerID'];
    $first_name = $_POST['FirstName'];
    $last_name = $_POST['LastName'];
    $address = $_POST['Address'];
    $city = $_POST['City'];
    $state = $_POST['State'];
    $zip = $_POST['Zip'];
    $phone = $_POST['Phone'];
    $email = $_POST['Email'];
    $password = $_POST['Password'];

    // DB stuff
    include('../Template/db_conn.php');
    try{

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("
                insert into phpclass.customers(CustomerID, FirstName, LastName, Address,                                        City, State, Zip, Phone, Email, Password)
                VALUE (:CustomerID, :FirstName, :LastName, :Address, :City, :State, :Zip, :Phone, :Email, :Password)
            ");

        $sql->bindValue(':CustomerID', $customer);
        $sql->bindValue(':FirstName', $first_name);
        $sql->bindValue(':LastName', $last_name);
        $sql->bindValue(':Address', $address);
        $sql->bindValue(':City', $city);
        $sql->bindValue(':State', $state);
        $sql->bindValue(':Zip', $zip);
        $sql->bindValue(':Phone', $phone);
        $sql->bindValue(':Email', $email);
        $sql->bindValue(':Password', $password);

        $sql->execute();

        // exit('DB SUCCESS!!');

        header("Location:customers.php?success=1");

    }
    catch(PDOException $e){
        echo "DB ERROR: " . $e->getMessage();
        exit;
    }
}elseif(isset($_POST) && !empty($_POST))
{
    $error = "Please ensure you have filled in all fields.";
}


?>






<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heather's Homepage</title>
    <link rel="stylesheet" type="text/css" href="mystyles.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <form method="post">
        <?php if (isset($error)) { ?>
            <p class="error"><?=$error;?></p>

        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">  Add New Customer </th>
            </tr>
            <tr height="50">
                <th>CustomerID</th>
                <td><input type="text" name="CustomerID" id="CustomerID" value="<?= $customer?>"/></td>
            </tr>
            <tr height="50">
                <th>FirstName</th>
                <td><input type="text" name="FirstName" id="FirstName" value="<?= $first_name ?>"/></td>
            </tr>
            <tr height="50">
                <th>LastName</th>
                <td><input type="text" name="LastName" id="LastName" value="<?= $last_name ?>"/></td>
            </tr>
            <tr height="50">
                <th>Address</th>
                <td><input type="text" name="Address" id="Address" value="<?= $address ?>"/></td>
            </tr>
            <tr height="50">
                <th>City</th>
                <td><input type="text" name="City" id="City" value="<?= $city ?>"/></td>
            </tr>
            <tr height="50">
                <th>State</th>
                <td><input type="text" name="State" id="State" value="<?= $state ?>"/></td>
            </tr>
            <tr height="50">
                <th>Zip</th>
                <td><input type="text" name="Zip" id="Zip" value="<?= $zip?>"/></td>
            </tr>
            <tr height="50">
                <th>Phone</th>
                <td><input type="tel" name="Phone" id="Phone" value="<?= $phone ?>"/></td>
            </tr>
            <tr height="50">
                <th>Email</th>
                <td><input type="email" name="Email" id="Email" value="<?= $email ?>"/></td>
            </tr>
            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="Password" id="Password" value="<?= $password ?>"/></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="customer_submit" id="customer_submit" value="Add Customer"
                </td>
            </tr>

        </table>


    </form>


</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>