<?php

include('../Template/db_conn.php');

try{

    $db = new PDO($db_dsn, $db_username, $db_password, $db_options);

    $sql = $db->prepare("SELECT * FROM customers;"); //taking order
    $sql->execute(); // baking order
    $rows = $sql->fetchAll(); // delivery



    // echo"<pre>";

    // print_r($rows); //eating the pizza

    // echo "</pre>";
}
catch (PDOException $e)
{
    echo $e->getMessage();
    exit;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movie List</title>
    <link rel="stylesheet" type="text/css" href="../customers/mystyles.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <h1>Customer List</h1>
    <?php if(isset($_GET['success'])) { ?>
        <p class="success">Customer Updated/Added Successfully</p>
    <?php } ?>
    <?php if(isset($_GET['delete']) && $_GET['delete'] == '1') { ?>
        <p class="success">Customer Deleted Successfully</p>
    <?php } else if (isset($_GET['delete']) && $_GET['delete'] == '0') { ?>
        <p class="error">Movie Could Not Be Deleted At This Time. Please Try Again</p>
    <?php } ?>

    <table border="1" >

        <tr>
            <th>CustomerID</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Password</th>
        </tr>

        <?php foreach($rows as $custom) {     ?>
        <tr>

            <td><a href="update.php?id=<?= $custom['CustomerID'] ?>"><?= $custom['CustomerID'] ?></a></td>
            <td><?= $custom['FirstName'] ?></td>
            <td><?= $custom['LastName'] ?></td>
            <td><?= $custom['Address'] ?></td>
            <td><?= $custom['City'] ?></td>
            <td><?= $custom['State'] ?></td>
            <td><?= $custom['Zip'] ?></td>
            <td class="phone"><?= $custom['Phone'] ?></td>
            <td><?= $custom['Email'] ?></td>
            <td><?= $custom['Password'] ?></td>
            <?php } ?>
        </tr>

    </table>
    <p>
        <a href="addcustomers.php"> Add New Customer</a>
    </p>


</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>