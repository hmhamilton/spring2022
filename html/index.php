<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heather's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>
<body>
    <header>
        <?php include ('Template/header.php'); ?>
    </header>

    <nav>
        <?php include('Template/nav.php'); ?>
    </nav>

    <main>
        <img src="images/puppy.jpg" alt="Heather's Image"/>
        <p>Mauris rhoncus hendrerit elit, a elementum elit suscipit porta. Phasellus fringilla nisl mattis aliquet sodales. Morbi suscipit dictum est, eget euismod urna elementum a. Aliquam vehicula pretium ante, non lobortis ante lacinia a. Quisque vel massa ut sem ornare finibus. Donec nec justo vitae arcu semper viverra vitae in diam. Nam nec lacinia nunc. Duis non tempus ipsum, et molestie erat. Proin congue diam id molestie porta. Praesent vitae finibus nibh. Vestibulum iaculis auctor enim, nec facilisis eros consectetur tempus. Ut rhoncus eleifend vestibulum.</p>

    </main>

    <footer>
        <?php include('Template/footer.php'); ?>
    </footer>


</body>
</html>