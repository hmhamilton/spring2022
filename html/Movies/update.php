<?php

    if( isset($_GET['id']) && !empty($_GET['id'])  && filter_var($_GET['id'], FILTER_VALIDATE_INT)
    )
    {
        $id = $_GET['id'];
        include ('../Template/db_conn.php');


        try {
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("SELECT * FROM phpclass.movielist where Movie_ID = :id");

            $sql->bindValue(":id", $id);
            $sql->execute();
            $row = $sql->fetch();

            if(!$row){
                header("location: list.php");
            }

            $title = $row['movie_title'];
            $rating = $row['movie_rating'];
        }
        catch(PDOException $e){
            echo $e->getMessage();
            exit;
        }

        //echo $title . ":" . $rating;




    }
    else
    {
        header("location: list.php");
    }
    if(
        isset($_POST['movie_name']) && !empty($_POST['movie_name'])
        && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])
        && isset($_POST['movie_id']) && !empty($_POST['movie_id'])
    ) {
       // echo "<pre>";        print_r($_POST);        echo "<pre>";        exit;

        $movie_title= $_POST['movie_name'];
        $movie_rating = $_POST['movie_rating'];
        $movie_id = $_POST['movie_id'];

        // DB stuff
        //include('../Template/db_conn.php');
        try{

            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("
                UPDATE phpclass.movielist set 
                movie_title = :Title, movie_rating = :Rating
                WHERE Movie_ID = :Id
            ");

            $sql->bindValue(':Title', $movie_title);
            $sql->bindValue(':Rating', $movie_rating);
            $sql->bindValue(':Id', $movie_id);
            $sql->execute();

           // exit('DB SUCCESS!!');

            header("Location:list.php?success=1");

        }
        catch(PDOException $e){
            echo "DB ERROR: " . $e->getMessage();
            exit;
        }
    }elseif(isset($_POST) && !empty($_POST))
    {
        $error = "Please ensure you have a title and rating before submitting the new movie.";
    }


?>






<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />

    <script type="text/javascript">

        function DeleteMovie(moviename, id)
        {


            if(confirm("Do you really want to delete " + moviename + "?")){
                // alert(moviename + " : " + id)
                document.location.href = "delete.php?id=" + id;



            }
        }
    </script>
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <form method="post">
        <input type="hidden" name="movie_id" id="movie_id" value="<?= $id ?>">
        <?php if (isset($error)) { ?>
            <p class="error"><?=$error;?></p>

        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
              <th colspan="2">  Update Movie </th>
            </tr>
            <tr height="50">
                <th>Movie Name</th>
                <td><input type="text" name="movie_name" id="movie_name" value="<?= $title?>" /></td>
            </tr>

            <tr height="50">
                <th>Movie Rating</th>
                <td><input type="text" name="movie_rating" id="movie_rating" value="<?= $rating?>" /></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="movie_submit" id="movie_submit" value="Update Movie"/>
                    <input type="button" name="movie_delete" id="movie_delete" value="Delete Movie"
                           onclick="DeleteMovie('<?= $title?>', '<?= $id ?>')"/>
                </td>
            </tr>

        </table>


    </form>


</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>