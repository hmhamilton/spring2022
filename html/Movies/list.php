<?php

    include('../Template/db_conn.php');

    try{

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);

        $sql = $db->prepare("SELECT * FROM phpclass.movielist;"); //taking order
        $sql->execute(); // baking order
        $rows = $sql->fetchAll(); // delivery

       // echo"<pre>";

       // print_r($rows); //eating the pizza

       // echo "</pre>";
    }
catch (PDOException $e)
{
    echo $e->getMessage();
    exit;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Movie List</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <h1>Movie List</h1>
    <?php if(isset($_GET['success'])) { ?>
        <p class="success">Movie Updated/Added Successfully</p>
    <?php } ?>
    <?php if(isset($_GET['delete']) && $_GET['delete'] == '1') { ?>
        <p class="success">Movie Deleted Successfully</p>
    <?php } else if (isset($_GET['delete']) && $_GET['delete'] == '0') { ?>
        <p class="error">Movie Could Not Be Deleted At This Time. Please Try Again</p>
    <?php } ?>
    <table border="1" width="80%">

        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Rating</th>
        </tr>

        <?php foreach($rows as $movie) {     ?>
        <tr>
            <td><?= $movie['Movie_ID'] ?></td>
            <td><a href="update.php?id=<?= $movie['Movie_ID'] ?>"><?= $movie['movie_title'] ?></a></td>
            <td><?= $movie['movie_rating'] ?></td>
        <?php } ?>
        </tr>

    </table>
    <p>
        <a href="add.php"> Add New Movie</a>
    </p>


</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>