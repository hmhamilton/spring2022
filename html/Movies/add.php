<?php

    if(
        isset($_POST['movie_name']) && !empty($_POST['movie_name'])
        && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])
    ) {
       // echo "<pre>";        print_r($_POST);        echo "<pre>";        exit;

        $title= $_POST['movie_name'];
        $rating = $_POST['movie_rating'];

        // DB stuff
        include('../Template/db_conn.php');
        try{

            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("
                insert into phpclass.movielist(movie_title, movie_rating)
                VALUE (:Title, :Rating)
            ");

            $sql->bindValue(':Title', $title);
            $sql->bindValue(':Rating', $rating);
            $sql->execute();

           // exit('DB SUCCESS!!');

            header("Location:list.php?success=1");

        }
        catch(PDOException $e){
            echo "DB ERROR: " . $e->getMessage();
            exit;
        }
    }elseif(isset($_POST) && !empty($_POST))
    {
        $error = "Please ensure you have a title and rating before submitting the new movie.";
    }


?>






<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heather's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>

<nav>
    <?php include('../Template/nav.php'); ?>
</nav>

<main>
    <form method="post">
        <?php if (isset($error)) { ?>
            <p class="error"><?=$error;?></p>

        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
              <th colspan="2">  Add New Movie </th>
            </tr>
            <tr height="50">
                <th>Movie Name</th>
                <td><input type="text" name="movie_name" id="movie_name" value="<?= $title ?>"/></td>
            </tr>

            <tr height="50">
                <th>Movie Rating</th>
                <td><input type="text" name="movie_rating" id="movie_rating" value="<?= $rating ?>"/></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="movie_submit" id="movie_submit" value="Add Movie"
                </td>
            </tr>

        </table>


    </form>


</main>

<footer>
    <?php include('../Template/footer.php'); ?>
</footer>


</body>
</html>